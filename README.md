# Java SE Presentation

This contains the javascript code for the Java SE presentation for ASE Bucharest, Romania.

This is done using flowtime.js, pretty cool library and a great alternative for the boring old power point presentation.

I have this running on an express server on port 9000.
